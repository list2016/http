package http;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class demo {

    public void execute() {

        HttpResponse<JsonNode> jsonResponse = null;
        try {
            jsonResponse = Unirest.post("https://httpbin.org/post")
                    .header("accept", "application/json")
                    .queryString("apiKey", "123")
                    .field("parameter", "value")
                    .asJson();
            printResponse(jsonResponse);
            System.out.println("----------------------------------------------------------------------");
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        try {
            HttpResponse<String> response = Unirest.get("https://yandex.ru")
                    .header("name1","value1")
                    .queryString("name2", "value1")
                    .asString();
            printResponse(response);
        } catch (UnirestException e) {
            e.printStackTrace();
        }

    }

    public void printResponse(HttpResponse<?> response){
        System.out.println(response.getHeaders());
        System.out.println(response.getStatusText());
        System.out.println(response.getRawBody());
        System.out.println(response.getBody());
        System.out.println(response.getStatus());
    }

}
